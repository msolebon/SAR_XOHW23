#!/bin/bash
APP_ROOT=$(dirname $(dirname $(readlink -fm $0)))
mkdir -p $APP_ROOT/output
mkdir -p $APP_ROOT/error
mkdir -p $APP_ROOT/log
if [ -z $3 ]; then NTHREADS=32; else NTHREADS=$3; fi
sbatch <<EOT
#!/bin/bash

#SBATCH --job-name="radar_openmp_$1_$2"
#SBATCH -D $APP_ROOT
#SBATCH --output=log/openmp_$1_$2_%j.log
#SBATCH --error=error/openmp_$1_$2_%j.err
#SBATCH --ntasks=$NTHREADS
#SBATCH --time=00:20:00
export OMP_NUM_THREADS=$NTHREADS

./bin/obpmark-1.2-radar_openmp -w $1 -h $2 -m 4 -F data/input_data/$1_$2/
EOT
