#!/bin/bash
APP_ROOT=$(dirname $(dirname $(readlink -fm $0)))
mkdir -p $APP_ROOT/output
mkdir -p $APP_ROOT/error
mkdir -p $APP_ROOT/log
sbatch <<EOT
#!/bin/bash

#SBATCH --job-name="radar_hipfft_$1_$2"
#SBATCH -D $APP_ROOT
#SBATCH --output=log/hipfft_$1_$2_%j.log
#SBATCH --error=error/hipfft_$1_$2_%j.err
#SBATCH --ntasks=1
#SBATCH --gres=gpu:1
#SBATCH --time=00:20:00

./bin/obpmark-1.2-radar_hipfft -w $1 -h $2 -m 4 -F data/input_data/$1_$2/
EOT
