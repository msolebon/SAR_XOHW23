#!/bin/bash
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

bash $SCRIPT_DIR/cpu.sh 1728 8815
bash $SCRIPT_DIR/cpu.sh 2752 14357
bash $SCRIPT_DIR/cpu.sh 4800 24487
bash $SCRIPT_DIR/cpu.sh 5616 28904

NT=64
bash $SCRIPT_DIR/omp.sh 1728 8815 $NT
bash $SCRIPT_DIR/omp.sh 2752 14357 $NT
bash $SCRIPT_DIR/omp.sh 4800 24487 $NT
bash $SCRIPT_DIR/omp.sh 5616 28904 $NT

bash $SCRIPT_DIR/hip.sh 1728 8815
bash $SCRIPT_DIR/hip.sh 2752 14357
bash $SCRIPT_DIR/hip.sh 4800 24487
bash $SCRIPT_DIR/hip.sh 5616 28904

bash $SCRIPT_DIR/hipfft.sh 1728 8815
bash $SCRIPT_DIR/hipfft.sh 2752 14357
bash $SCRIPT_DIR/hipfft.sh 4800 24487
bash $SCRIPT_DIR/hipfft.sh 5616 28904

bash $SCRIPT_DIR/ocl.sh 1728 8815
bash $SCRIPT_DIR/ocl.sh 2752 14357
bash $SCRIPT_DIR/ocl.sh 4800 24487
bash $SCRIPT_DIR/ocl.sh 5616 28904
