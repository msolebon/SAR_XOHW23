#!/bin/bash

make
cd matlab
/mnt/d/Program\ Files/MATLAB/bin/matlab.exe -batch InputGen
cd ../

./datagen-1.2-radar -i matlab/data/small/data.bin -p matlab/data/small/params.bin
./datagen-1.2-radar -i matlab/data/mid/data.bin -p matlab/data/mid/params.bin
./datagen-1.2-radar -i matlab/data/big/data.bin -p matlab/data/big/params.bin
./datagen-1.2-radar -i matlab/data/full/data.bin -p matlab/data/full/params.bin

