% This function is used to create azimuth reference function and to calculate no. of line in a aperture.
function validAzPts = compute_validAZ(doppler,nValid,fs,ro,vEff,lambda,prf,numLines)
% doppler : doppler centroid frequency
% nValid : no. of valid range sample
% fs : range sampling frequency
% ro : near slant range
% vEff : effective velocity of sensor
% lambda : wavelength
% prf : pulse repetition frequency
% numLines : no. of lines in a patch
L=10;
c= 2.99792458e+08;
range = ro + [0:nValid-1] * (c/(2*fs)); % computes range perpendicular to azimuth direction 
rdc = range/sqrt(1-(lambda*doppler/(2*vEff))^2); % squinted range 
az_beamwidth = rdc * (lambda/L) * 0.8; % use only 80%  
tau_az = az_beamwidth / vEff; % azimuth pulse length 
nPtsAz = ceil(tau_az(end) * prf); % use the far range value
validAzPts = numLines - nPtsAz ;  % valid azimuth points  