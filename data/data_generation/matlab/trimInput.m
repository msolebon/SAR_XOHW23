function [trimmed, patchSize] = trimInput(data, Size, xOffset, yOffset, patch)
[s1,s2] = size(data);

patchSize = [patch(1), Size];
extraR = s2-patch(2);
mul=(s1/s2);
ySize = yOffset+1:yOffset+Size+extraR;
xSize = floor(xOffset*mul)+1:floor((xOffset+Size+extraR)*mul);

trimmed = data(xSize, ySize);

end

