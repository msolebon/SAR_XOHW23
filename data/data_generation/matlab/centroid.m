% This function is used to estimate doppler centroid frequency using
% Average phase change method.
function fdc=centroid(data,prf)
% data : range compressed data
% prf : pulse repetition frequency
sumLines = sum(data(2:end,:) .* conj(data(1:end-1,:)));
sumLines = sum(sumLines)/length(data);
avgPhChg = atan2(imag(sumLines),real(sumLines));
fdc = mean(avgPhChg)/(2*pi)*prf; % doppler centroid [Hz]