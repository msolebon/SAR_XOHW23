function store_data(test,rawData,fs, fc, prf, tau, v, ro, slope, patch, sizes)

[asize, rsize] = size(rawData);
mydir = fullfile(pwd, "data");
mydir = fullfile(mydir, test);
if ~exist(mydir,'dir')
    mkdir(mydir);
end
lambda=freq2wavelen(fc);
lines = pow2(ceil(log2(sizes(1)+1)));
params = [lambda, prf, tau, fs, v, ro, slope];
integers = [asize, sizes(1), lines, rsize, sizes(2), patch];
par = fullfile(mydir,'params.bin');
fileID = fopen(par,'wb');
fwrite(fileID,params,'float');
fwrite(fileID,integers,'uint');
fclose(fileID);

rawr = real(rawData);
rawi = imag(rawData);
out = zeros(asize,rsize*2);
out(:,1:2:end) = rawr;
out(:,2:2:end) = rawi;
out = out';

dat = fullfile(mydir, 'data.bin');
fileID = fopen(dat,'wb');
fwrite(fileID,out,'float');
fclose(fileID);
end

