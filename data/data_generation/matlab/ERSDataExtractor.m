function [rawData, doppler, patchSize]= ERSDataExtractor(filename,fs,fc,prf,tau,v,ro)
c = physconst('LightSpeed');                    % Speed of light
lambda = freq2wavelen(fc);
%% Extract data
numLines = 2048;
numBytes = 11644;
numHdr = 412;
nValid= (numBytes-numHdr)/2 - round(tau*fs);  % valid range samples

fid = fopen(sprintf('%s.raw',filename),'r');
fseek(fid,numBytes,'bof');     % skipping first line
clear rawData;

%%first iteration
fseek(fid,11644,'cof');   
data = fread(fid,[numBytes,numLines],'uint8')'; % reading raw data file

% interpret as complex values and remove mean
data = complex(data(:,numHdr+1:2:end),data(:,numHdr+2:2:end));
% Remove mean
data = data - mean(data(:)); % remove mean = 15.5  
dwidth = size(data,2);
% determine the Doppler centroid using average phase change method
doppler=centroid(data,prf);
% Azimuth valid input
validAzPts=compute_validAZ(doppler,nValid,fs,ro,v,lambda,prf,numLines);
tot_patch = floor(28652/validAzPts);

rawData((1:validAzPts)+((1-1)*validAzPts),1:dwidth) = data(1:validAzPts,1:dwidth);
fseek(fid,numBytes,'bof');
fseek(fid,numBytes*validAzPts*1,'cof');
%%loop body
for h=2:tot_patch    
fseek(fid,11644,'cof');   
data = fread(fid,[numBytes,numLines],'uint8')'; % reading raw data file
% interpret as complex values and remove mean
data = complex(data(:,numHdr+1:2:end),data(:,numHdr+2:2:end));
% Remove mean
data = data - mean(data(:)); % remove mean = 15.5  

rawData((1:validAzPts)+((h-1)*validAzPts),1:dwidth) = data(1:validAzPts,1:dwidth);
fseek(fid,numBytes,'bof');
fseek(fid,numBytes*validAzPts*h,'cof');

end
%%extra
fseek(fid,11644,'cof');   
data = fread(fid,[numBytes,numLines],'uint8')'; % reading raw data file
% interpret as complex values and remove mean
data = complex(data(:,numHdr+1:2:end),data(:,numHdr+2:2:end));
% Remove mean
data = data - mean(data(:)); % remove mean = 15.5  
remain = size(data,1);
rawData((1:remain)+((26-1)*validAzPts),1:dwidth) = data(1:remain,1:dwidth);

patchSize = [validAzPts, nValid];

fclose(fid);


end