
%% Download data
filename = 'E2_84699_STD_L0_F299.000';
downloadFolder = fullfile(pwd, 'download');
if ~exist(downloadFolder,'dir')
	mkdir(downloadFolder);
end
dataURL=['https://ssd.mathworks.com/supportfiles/radar/data/'...
	'ERSData.zip'];
helperDownloadERSData(downloadFolder, dataURL);
filename = fullfile(downloadFolder, filename);
%% Extract parameters
fprintf("Extracting parameters...");
[fs,fc,prf,tau,bw,v,ro,fd] = ERSParameterExtractor(sprintf('%s.ldr', filename));
slope = 4.19e11;           % Slope of the transmitted chirp (Hz/s)
fprintf("Done\n");

%% Extract data
fprintf("Extracting raw data...");
[rawData, fDc, patchSize] = ERSDataExtractor(filename,fs,fc,prf,tau,v,ro);
fprintf("Done\n");

%% Choose test profile (small, mid, big, full)
for test = ["small" "mid" "big" "full"]
    fprintf("Creating files for %s test...", test);
	%% Trim data to test size
	if test == "full"
		[trimmedData, trimmedSize] = trimInput(rawData, 4912, 0, 0, patchSize);
	elseif test == "big"
		[trimmedData, trimmedSize] = trimInput(rawData, 4096, 0, 0, patchSize);
	elseif test == "mid"
		[trimmedData, trimmedSize] = trimInput(rawData, 2048, 850, 1000, patchSize);
	elseif test == "small"
		[trimmedData, trimmedSize] = trimInput(rawData, 1024, 850, 1000, patchSize);
	end
	[asize, rsize] = size(trimmedData);
	validAzPts = trimmedSize(1);
	nValid = trimmedSize(2);
	numLines = pow2(ceil(log2(validAzPts)));
	tot_patch=floor(asize/validAzPts); 

	extraZ = (tot_patch-1)*validAzPts+numLines-asize;
       	%add zeros to prcess last patch
	trimmedData = [trimmedData' zeros(rsize,extraZ)]';
	[asize, rsize] = size(trimmedData);

	%% Store data and parameters to disk
	store_data(test, trimmedData,fs, fc, prf, tau, v, ro, slope, tot_patch, trimmedSize);
    fprintf(" Done\n");
end
